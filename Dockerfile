FROM node:latest AS build-amd64
WORKDIR /app
COPY . /app
RUN npm install
RUN npm run vue:build

FROM node:latest AS bin-amd64
ENV APP_PORT=3000
ENV NODE_ENV=production
WORKDIR /app
COPY package.json /app
RUN npm install
COPY --from=build-amd64 /app/public /app/public
COPY index.js /app
ENTRYPOINT [ "npm", "start" ]

FROM node:latest AS build-arm
WORKDIR /app
COPY . /app
RUN npm install
RUN npm run vue:build

FROM node:latest AS bin-arm
ENV APP_PORT=3000
ENV NODE_ENV=production
WORKDIR /app
COPY package.json /app
RUN npm install
COPY --from=build-arm /app/public /app/public
COPY index.js /app
ENTRYPOINT [ "npm", "start" ]

EXPOSE 3000