const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const low = require("lowdb");
const FileSync = require("lowdb/adapters/FileSync");
const shortid = require("shortid");

const adapter = new FileSync("db.json");
const db = low(adapter);

const PORT = process.env.PORT || process.env.APP_PORT || 3000;

db.defaults({
  items: []
}).write();

const app = express();

app.use(cors());

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get("/api/items", (req, res) => {
  let items = db.get("items").value();
  res.json(items);
});

app.post("/api/items", (req, res) => {
  let id = db
    .get("items")
    .push({
      id: shortid.generate(),
      need: true,
      name: "",
      desc: req.body.name
    })
    .write().id;
  res.json({ success: true, id });
});

app.put("/api/items", (req, res) => {
  let items = req.body.items;
  for (let i of items) {
    db.get("items")
      .find({ id: i.id })
      .assign(i)
      .write();
  }
  res.json({ success: true });
});

app.delete("/api/items/:id", (req, res) => {
  let id = req.params.id;
  db.get("items")
    .remove({ id })
    .write();
  res.json({ success: true });
});

app.use("/", express.static("public"));

app.listen(PORT, () => console.log(`Listening to port ${PORT}`));
